from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from re import search

class PagesTest(StaticLiveServerTestCase):
    
    def setUp(self):
        self.browser = webdriver.Firefox()


    def tearDown(self):
        self.browser.quit()

    def test_user_can_visit_home_page(self):
        self.browser.get(self.live_server_url)

        self.assertIn('Home', self.browser.title)

        text_found = search(r'healthy', self.browser.page_source)
        self.assertNotEqual(text_found, None)

    
    def test_user_can_visit_about_page(self):
        self.browser.get(self.live_server_url + reverse('pages:about'))

        self.assertIn('About', self.browser.title)

        text_found = search(r'Our TeaCorp Store', self.browser.page_source)
        self.assertNotEqual(text_found, None)

        text_found = search(r'Team Members', self.browser.page_source)
        self.assertNotEqual(text_found, None)

    
    def test_user_can_visit_contact_page(self):
        self.browser.get(self.live_server_url + reverse('pages:contact'))

        self.assertIn('Contact Us', self.browser.title)

        text_found = search(r'Contact Form', self.browser.page_source)
        self.assertNotEqual(text_found, None)

        text_found = search(r'Contact Address', self.browser.page_source)
        self.assertNotEqual(text_found, None)

        fullname_inputbox = self.browser.find_element_by_id('id_fullname')
        email_inputbox = self.browser.find_element_by_id('id_email')
        subject_inputbox = self.browser.find_element_by_id('id_subject')
        message_inputbox = self.browser.find_element_by_id('id_message')

        fullname_inputbox.send_keys('Test Singh')
        email_inputbox.send_keys('test@gmail.com')
        subject_inputbox.send_keys('Test')
        message_inputbox.send_keys('Test message here...')

        contact_us_btn = self.browser.find_element_by_id('contact_us_btn')

        contact_us_btn.send_keys(Keys.ENTER)

        self.assertIn('Thank you', self.browser.page_source)





