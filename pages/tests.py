from django.test import TestCase
from django.urls import reverse

from shop.helper import ShopHelper
from . forms import ContactForm

shop_helper = ShopHelper()

class HomePageTests(TestCase):

    def test_visit_home_view_with_url_name(self):
        response = self.client.get(reverse('pages:home'))

        self.assertEqual(response.status_code, 200)

    def test_home_view_uses_correct_template(self):
        response = self.client.get(reverse('pages:home'))

        self.assertTemplateUsed(response, 'pages/home.html')

    def test_home_view_contains_featured_products(self):
        shop_helper.create_products_for_testing(num_of_products=4)

        response = self.client.get(reverse('pages:home'))

        self.assertEqual(response.context['featured_products'].count(), 4)


class AboutPageTest(TestCase):
    
    def test_visit_about_view_with_url_name(self):
        response = self.client.get(reverse('pages:about'))

        self.assertEqual(response.status_code, 200)

    def test_about_view_uses_correct_template(self):
        response = self.client.get(reverse('pages:about'))

        self.assertTemplateUsed(response, 'pages/about.html')


class ContactUsPageTest(TestCase):

    def test_visit_contact_us_view_with_url_name(self):
        response = self.client.get(reverse('pages:contact'))

        self.assertEqual(response.status_code, 200)

    def test_contact_us_view_uses_correct_template(self):
        response = self.client.get(reverse('pages:contact'))

        self.assertTemplateUsed(response, 'pages/contact.html')

    def test_can_send_contact_us_email(self):
        
        contact_form = ContactForm()
        contact_form.fullname = 'Manpreet Singh'
        contact_form.email = 'test@gmail.com'
        contact_form.subject = 'Test Subject'
        contact_form.message = 'Test message'

        response = self.client.post(
            reverse('pages:contact'),
            contact_form.__dict__
        )

        self.assertRedirects(response, reverse('pages:contact'))


