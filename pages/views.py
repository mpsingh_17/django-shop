from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from django.contrib import messages

from shop.models import Product
from . forms import ContactForm


class HomeView(generic.ListView):
    HOME_PAGE_PRODUCT_COUNT = 4
    
    template_name = 'pages/home.html'
    model = Product
    queryset = Product.objects.all().order_by('-created')[:HOME_PAGE_PRODUCT_COUNT]
    context_object_name = 'featured_products'


class AboutView(generic.TemplateView):
    template_name = 'pages/about.html'



class ContactUsView(generic.edit.FormView):
    template_name = 'pages/contact.html'
    form_class = ContactForm
    success_url = reverse_lazy('pages:contact')


    def form_valid(self, form):
        form.send_email()
        messages.success(self.request, 'Thank you for contacting us.')
        return super().form_valid(form)


