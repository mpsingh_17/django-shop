from . models import Product, Category
from decimal import Decimal
import random

class ShopHelper():

    def create_products_for_testing(self, num_of_products, is_featured=False, category=None):

        if category is None:
            category = self.create_category_for_testing()

        for x in range(num_of_products):
            Product.objects.create(
                category=category,
                name=f'test-{x}',
                slug=f'test-{x}',
                price=Decimal(2) + Decimal(x),
            )


    def create_category_for_testing(self):
        return Category.objects.create(
            name='test_category',
            slug='test-category',
        )

    # Returns list of one or more products.
    def get_random_products(self, num_of_products):
        products = Product.objects.all()
        return random.sample(list(products), num_of_products)
