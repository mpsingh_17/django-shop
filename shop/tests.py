from django.test import TestCase
from django.urls import reverse
import random

from . helper import ShopHelper

shop_helper = ShopHelper()

class ProductListTest(TestCase):

    def test_can_visit_product_list_view_with_named_url(self):
        response = self.client.get(reverse('shop:product_list'))

        self.assertEqual(response.status_code, 200)

    def test_product_list_view_uses_correct_template(self):
        response = self.client.get(reverse('shop:product_list'))

        self.assertTemplateUsed(response, 'shop/product/list.html')

    def test_response_contains_products_key(self):
        response = self.client.get(reverse('shop:product_list'))

        self.assertTrue('products' in response.context)

    def test_response_contains_atleast_one_product(self):
        shop_helper.create_products_for_testing(num_of_products=2)

        response = self.client.get(reverse('shop:product_list'))

        products_count = response.context['products'].count()
        self.assertGreater(products_count, 1)

    
    def test_response_contains_products_key_when_specific_category(self):
        category = shop_helper.create_category_for_testing()
        shop_helper.create_products_for_testing(num_of_products=2, category=category)

        response = self.client.get(reverse('shop:product_list_by_category', args=[category.slug]))
        self.assertTrue('products' in response.context)


    def test_can_display_specific_category_products(self):
        category = shop_helper.create_category_for_testing()
        shop_helper.create_products_for_testing(num_of_products=2, category=category)

        response = self.client.get(reverse('shop:product_list_by_category', args=[category.slug]))

        products_count = response.context['products'].count()
        self.assertGreater(products_count, 1)


class ProductDetailTest(TestCase):

    def request_product_detail_view(self, num_of_products_to_create):
        shop_helper.create_products_for_testing(num_of_products_to_create)
        product = shop_helper.get_random_products(1)
        return self.client.get(reverse('shop:product_detail', args=[product[0].id, product[0].slug]))


    def test_can_visit_product_detail_view_with_named_url(self):
        response = self.request_product_detail_view(num_of_products_to_create=5)

        self.assertEqual(response.status_code, 200)

    def test_product_detail_view_uses_correct_template(self):
        response = self.request_product_detail_view(num_of_products_to_create=5)
        self.assertTemplateUsed(response, 'shop/product/detail.html')

    def test_response_contains_product_key(self):
        response = self.request_product_detail_view(num_of_products_to_create=5)
        self.assertTrue('product' in response.context)

    def test_response_contains_the_product_we_requested(self):
        shop_helper.create_products_for_testing(5)
        product = shop_helper.get_random_products(1)
        response = self.client.get(reverse('shop:product_detail', args=[product[0].id, product[0].slug]))

        self.assertEqual(product[0], response.context['product'])



