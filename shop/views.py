from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from . models import Category, Product
from cart.forms import CartAddProductForm



class ProductList(ListView):
    model = Product
    template_name = 'shop/product/list.html'
    context_object_name = 'products'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        categories = Category.objects.all()
        category_slug = self.kwargs.get('category_slug', None)

        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)    
            context['products'] = context['products'].filter(category=category)
            context['categories'] = categories
            context['category'] = category
        
        return context



class ProductDetails(DetailView):
    template_name = 'shop/product/detail.html'
    model = Product
    context_object_name = 'product'


